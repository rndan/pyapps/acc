#!/bin/bash

# Usage python3 runacc.py [dst_ip] 
# i.e
# python3 runacc.py 192.168.0.107

IMG='rpi64'
APP='acc'
UPORT=5001

UNAME_M=$(uname -m)
if [[ "$UNAME_M" == 'armv7l' ]]; then
   IMG='rpi32'
elif [[ "$UNAME_M" == 'x86_64' ]]; then
   IMG='amd64'
fi

echo $APP:$IMG at port $UPORT

if [ -z "$1" ]
  then
    docker run -it -e BROKER=192.168.0.107 -e DPORT=$UPORT -e PROTO=udp -p $UPORT:$UPORT/udp registry.gitlab.com/rndan/rpi-apps/$APP:$IMG
  else
    docker run -it -e BROKER=$1 -e DPORT=$UPORT -e PROTO=udp -p $UPORT:$UPORT/udp registry.gitlab.com/rndan/rpi-apps/$APP:$IMG
fi
