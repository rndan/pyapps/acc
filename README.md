# MQTT Publisher of accelerometer data

# Ref.
https://gitlab.com/rndan/rpi-apps/gps

# Incomming accelerometer data
Accelerometer data expected in JSONs like:\
{"accelerometer":{"value":[0.82832336,8.573624,4.7326813]}}\
Where acceleromer data in value array are in order: acc_x, acc_y, acc_z
