# FROM ubuntu:22.04
# for RPI 32 bit:
# FROM ubuntu:18.04

# RUN apt-get update && apt install -y python3-pip python3-netifaces && rm -rf /usr/lib/python3.*/EXTERNALLY-MANAGED && pip3 install "paho-mqtt<2.0.0"
# if fails try pip install without flag --break-system-packages
# RUN apt update && apt install -y python3-pip python3-netifaces && pip3 install "paho-mqtt<2.0.0"

FROM alpine:3.19
RUN apk update && apk add py3-pip py3-netifaces && rm -rf /usr/lib/python3.*/EXTERNALLY-MANAGED && pip3 install "paho-mqtt<2.0.0"

ENV BROKER=broker.emqx.io
ENV TOPIC=in
ENV DPORT=5001
ENV DTYPE=data
ENV PROTO=udp
ENV DEVID=rpi0acc

COPY accpub.py /home/accpub.py

CMD [ "sh", "-c", "python3 /home/accpub.py $BROKER $TOPIC $DPORT $DTYPE $PROTO $DEVID"]
