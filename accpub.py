# run with python3

import json
import netifaces
import math
import socket
import random
import time
import sys
from paho.mqtt import client as mqtt_client

# to use paho-mqtt 1.X install older version:
# pip3 install "paho-mqtt<2.0.0"
# to use with paho-mqtt 2.X change commented lines with new API  install it
# pip3 install paho-mqtt
# https://eclipse.dev/paho/files/paho.mqtt.python/html/migrations.html

# usage:
# python3 accpub.py [broker] [topic] [port] [type] [proto] [deviceid]
# python3 accpub.py 127.0.0.1 in 2947 data tcp rpi0
# or
# python3 accpub.py 192.168.0.107

# default broker, mqtt port, topic, data type, udp port to listen, mode protocol, device ID (configure from file?)
broker = 'broker.emqx.io'
port = 1883
srv = 5001  # server port to listen
topic = 'in'
#type = 'raw'
type = 'data'
proto = 'fake' # ws/udp/tcp/fake

# Generate a Client ID with the publish prefix.
nifs = netifaces.interfaces()
sensId = 'acc'
deviceId = f'publish-{sensId}-{random.randint(0, 1000)}'

def validate (text):
    try:
        j = json.loads(text)
        return text
    except ValueError as e:
        print('invalid json: %s' % e)
        return ""

def getmac(interface):
    try:
        mac = open('/sys/class/net/'+interface+'/address').readline()
    except:
        mac = "00:00:00:00:00:00"
    return mac[0:17]

for nif in nifs:
  if nif != "lo" and nif :
    deviceId = f'{getmac(nif)}-{sensId}'
    break

# username = 'emqx'
# password = 'public'

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      broker = sys.argv[pos]
    if pos  == 2 :
      topic =  sys.argv[pos]
    if pos == 3 :
       srv = int(sys.argv[pos])
    if pos == 4 :
      t = sys.argv[pos]
      if t == "raw" or t == "data" or t == "none" :
        type = t
      else :
        print(f"Warning: wrong data type '{t}': use data or raw or none. Set to default '{type}'")
    if pos == 5 :
      t = sys.argv[pos]
      if t == "ws" or t == "udp" or t == "tcp" or t == "fake" :
        proto = t
      else :
        print(f"Warning: wrong proto mode '{t}': use tcp or fake or ws or udp. Set to default '{proto}'")
    if pos == 6 :
        deviceId = sys.argv[pos]
    pos = pos + 1

client_id = deviceId

print (f"MQTT broker: '{broker}' topic '{topic}' deviceID '{client_id}' listen port {srv} type '{type}' proto = '{proto}'")

# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)

    # for never paho-mqtt >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)

    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def publish(client):
    msg_count = 0
    accin = '{"accelerometer":{"value":[0.823822,8.935364,4.063919],"timestamp":1713173514761181207}}'

    server_socket = None
    address = None
    connection = None

    if proto == 'udp' :
      server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      server_socket.bind(('', srv))

    if proto == 'tcp' :
      server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      server_address = ('0.0.0.0', srv)
      server_socket.bind(server_address)
      # Listen for incoming connections
      server_socket.listen(3)
      client_address = 0
      connected = False

      while True :
        try:
          print('tcp server listen at ' + str(srv))
          connection, client_address = server_socket.accept()
          print(f"new connection from {client_address}")
          addrc =  client_address[1]
          if  addrc != 0 :
            while addrc > 0:
              try:
                jmsg = connection.recv(1024)
                if len(jmsg) == 0 :
                  addrc = -1
                  break # listen again to new connection
                raw = jmsg.decode("utf-8")
                print(f"\nGet: {raw}")

                # create JSON and publish to MQTT
                obj = time.gmtime(0)
                epoch = time.asctime(obj)
                curr_time = round(time.time()*1000)
                pl = {}
                pl['ver'] = 1.0
                pl['app'] =  sys.argv[0]
                pl['deviceid'] = deviceId
                pl['ts'] = curr_time
                pl['status'] = 'on'
                pl['sensor'] = 'acc'

                if type == "none" :
                  pl['type'] = 'none'
                  ready = True

                if type == 'raw' :
                  if "accelerometer" in raw:
                    pl['type'] = 'raw'
                    pl['raw']  = raw
                    ready = True

                if type ==  'data' :
                  pl['type'] = 'data'
                  # interested in acc x,y,z:
                  jraw = validate (raw)
                  if jraw.count("accelerometer") == 1 :
                    accj = json.loads(jraw)
                    value = accj["accelerometer"]["value"]
                    # acc data parser
                    rnd = random.uniform(0.0, 0.5)
                    x = value[0] + rnd
                    y = value[1] - rnd
                    z = value[2] - rnd
                    dt = {}
                    dt['acc3d'] = math.sqrt(x*x + y*y + z*z)
                    pl['data'] =  dt
                    ready = True

                msg = json.dumps(pl)
                if ready:
                  ready = False
                  result = client.publish(topic, msg)
                  # result: [0, 1]
                  status = result[0]
                  if status == 0:
                    print(f"\nReceived:\n{raw} from '{client_address}' parsed '{jraw}' and sent {msg_count} to topic '{topic}':\n{msg}")
                  else:
                    print(f"Failed to send message to topic {topic}")
                  msg_count += 1

              except KeyboardInterrupt:
                addrc = -2
          if addrc == -1 :
            print("zero data len received at socket")
          if addrc == -2 :
            print("exit by Ctrl-C")
            server_socket.close()
            sys.exit(0)
        except KeyboardInterrupt:
          server_socket.close()
          print()
          sys.exit(0)
      # end tcp

    # proto: udp or fake
    while True:
      ready = False
      obj = time.gmtime(0)
      epoch = time.asctime(obj)
      curr_time = round(time.time()*1000)

      if proto == 'udp' :
        msg, address = server_socket.recvfrom(1024)
        raw = msg.decode("utf-8")

      # if proto == 'ws' :
        #TODO websocket server
        #raw = accin

      if proto == 'fake' or proto == 'udp' :
        if proto == 'fake' :
          raw = accin # use fake data
          address = 'fake'
          time.sleep(1.0)

        pl = {}
        pl['ver'] = 1.0
        pl['app'] =  sys.argv[0]
        pl['deviceid'] = deviceId
        pl['ts'] = curr_time
        pl['status'] = 'on'
        pl['sensor'] = 'acc'

        if type == "none" :
            pl['type'] = 'none'
            ready = True

        if type == 'raw' :
          if "accelerometer" in raw:
            pl['type'] = 'raw'
            pl['raw']  = raw
            ready = True

        if type ==  'data' :
           pl['type'] = 'data'
           # interested in acc x,y,z:
           jraw = validate (raw)
           if jraw.count("accelerometer") == 1 :
            accj = json.loads(jraw)
            value = accj["accelerometer"]["value"]
            # acc data parser
            rnd = 0.0
            if proto == 'fake': # add some fluctuation to fake acc data
              rnd = random.uniform(0.0, 0.5)
            x = value[0] + rnd
            y = value[1] - rnd
            z = value[2] - rnd
            dt = {}
            dt['acc3d'] = math.sqrt(x*x + y*y + z*z)
            pl['data'] =  dt
            ready = True

        msg = json.dumps(pl)
        if ready:
          ready = False
          result = client.publish(topic, msg)
          # result: [0, 1]
          status = result[0]
          if status == 0:
            print(f"\nReceived:\n{raw} from {address} parsed '{accin}' and sent {msg_count} to topic '{topic}':\n{msg}")
          else:
            print(f"Failed to send message to topic {topic}")
          msg_count += 1
    # end proto: udp/fake

if __name__ == '__main__':
    # for paho-mqtt  version >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)
    client = connect_mqtt()
    client.loop_start()
    publish(client)
    client.loop_stop()
